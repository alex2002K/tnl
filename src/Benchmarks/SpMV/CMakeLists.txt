# CSR5 does not work properly yet:
#
# https://github.com/weifengliu-ssslab/Benchmark_SpMV_using_CSR5/issues/9
# https://github.com/weifengliu-ssslab/Benchmark_SpMV_using_CSR5/issues/10
#
# We can build it with TNL but it crashes with many CUDA errors. We should first check it
# with the original build.
#
#include( cmake/BuildCSR5.cmake )

# PETSc requires MPI
#if( TNL_BUILD_MPI AND TNL_USE_PETSC )
#   find_package( PETSc COMPONENTS CXX )
#endif()

#if( PETSC_FOUND )
#   message( "PETSC library found: ${PETSC_VERSION}")
#   set( PETSC_CXX_FLAGS -DHAVE_PETSC ${PETSC_DEFINITIONS} )
#   message( "   PETSC_INCLUDES  = ${PETSC_INCLUDES}" )
#   message( "   PETSC_CXX_FLAGS = ${PETSC_CXX_FLAGS}" )
#   message( "   PETSC_LIBRARIES = ${PETSC_LIBRARIES}" )
#endif()

if( TNL_BUILD_CUDA )
   add_executable( tnl-benchmark-spmv tnl-benchmark-spmv.cu )
   target_link_libraries( tnl-benchmark-spmv PUBLIC TNL::TNL_CUDA )

   add_executable( tnl-benchmark-spmv-legacy tnl-benchmark-spmv-legacy.cu )
   target_link_libraries( tnl-benchmark-spmv-legacy PUBLIC TNL::TNL_CUDA )

   add_executable( tnl-benchmark-spmv-reference tnl-benchmark-spmv-reference.cu )
   target_link_libraries( tnl-benchmark-spmv-reference PUBLIC TNL::TNL_CUDA )
   find_package( CUDAToolkit REQUIRED )
   target_link_libraries( tnl-benchmark-spmv-reference PUBLIC CUDA::cusparse )
else()
   add_executable( tnl-benchmark-spmv tnl-benchmark-spmv.cpp )
   target_link_libraries( tnl-benchmark-spmv PUBLIC TNL::TNL_CXX )

   add_executable( tnl-benchmark-spmv-legacy tnl-benchmark-spmv-legacy.cpp )
   target_link_libraries( tnl-benchmark-spmv-legacy PUBLIC TNL::TNL_CXX )

   add_executable( tnl-benchmark-spmv-reference tnl-benchmark-spmv-reference.cpp )
   target_link_libraries( tnl-benchmark-spmv-reference PUBLIC TNL::TNL_CXX )
endif()

#target_compile_options( tnl-benchmark-spmv-reference PRIVATE ${PETSC_CXX_FLAGS} )
#target_include_directories( tnl-benchmark-spmv-reference PRIVATE ${PETSC_INCLUDES} )
#target_link_libraries( tnl-benchmark-spmv-reference ${PETSC_LIBRARIES} )

if( TNL_BUILD_MPI )
   find_package(Hypre)
   if( HYPRE_FOUND )
      # enable MPI support in TNL
      target_compile_definitions( tnl-benchmark-spmv-reference PUBLIC "-DHAVE_MPI" )
      # add MPI to the target: https://cliutils.gitlab.io/modern-cmake/chapters/packages/MPI.html
      target_link_libraries( tnl-benchmark-spmv-reference PUBLIC MPI::MPI_CXX )

      # enable Hypre support in TNL
      target_compile_definitions( tnl-benchmark-spmv-reference PUBLIC "-DHAVE_HYPRE" )
      # add Hypre to the target
      target_include_directories( tnl-benchmark-spmv-reference SYSTEM PUBLIC ${HYPRE_INCLUDE_DIRS} )
      target_link_libraries( tnl-benchmark-spmv-reference PUBLIC ${HYPRE_LIBRARIES} -lm )
   endif()
endif()

install( TARGETS tnl-benchmark-spmv RUNTIME DESTINATION bin )
install( TARGETS tnl-benchmark-spmv-legacy RUNTIME DESTINATION bin )
install( TARGETS tnl-benchmark-spmv-reference RUNTIME DESTINATION bin )
