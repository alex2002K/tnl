#include "DenseMatricesBenchmark.h"
#include "src/Algorithms/DenseMatrices/DenseMatrix.h"
#include "Config/parseCommandLine.h"
#include "Devices/Host/HostDevice.h"
#include "Devices/Cuda/CudaDevice.h"


int main(int argc, char* argv[]) {
    TNL::Config::ConfigDescription config;
    TNL::Devices::Host::configSetup(config);
    TNL::Devices::Cuda::configSetup(config);
    TNL::Config::ParameterContainer parameters;

    if (!TNL::parseCommandLine(argc, argv, config, parameters))
        return EXIT_FAILURE;

    if (!TNL::Devices::Host::setup(parameters) || !TNL::Devices::Cuda::setup(parameters))
        return EXIT_FAILURE;

    // Provide matrices A, B, and C, and their size
    int N = 1000; // Matrix size (N x N)
    float* A = new float[N * N];
    float* B = new float[N * N];
    float* C = new float[N * N];

    // Call your benchmark function here
    bool success = startBenchmark<float>(parameters, A, B, C, N);

    // Cleanup and return appropriate status
    delete[] A;
    delete[] B;
    delete[] C;

    return success ? EXIT_SUCCESS : EXIT_FAILURE;
}
