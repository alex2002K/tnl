#pragma once

#include <cstddef>

// Mocked MAGMA functions and data structures
typedef struct {
    int dummy;
} magma_queue_t;

void magma_queue_create(int device, magma_queue_t** queue);
void magma_queue_destroy(magma_queue_t* queue);

void magma_dmalloc(double** ptr, size_t size);
void magma_free(double* ptr);

void magma_setmatrix(int m, int n, size_t elemSize, const double* h_A, int ld, double* d_A, int ldda, magma_queue_t queue);
void magma_getmatrix(int m, int n, size_t elemSize, const double* d_A, int ldda, double* h_A, int ld, magma_queue_t queue);

void magma_dgemm(char transa, char transb, int m, int n, int k, double alpha, const double* A, int lda, const double* B, int ldb, double beta, double* C, int ldc, magma_queue_t queue);
