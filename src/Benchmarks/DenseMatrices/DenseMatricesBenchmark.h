#pragma once

#include <TNL/Config/parseCommandLine.h>
#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Algorithms/DenseMatrices/DenseMatrix.h>
#include <TNL/Algorithms/DenseMatrices/DenseMatrices.h>
#include <TNL/Algorithms/DenseMatrices/DenseMatrixOperations.h>
#include <TNL/Algorithms/DenseMatrices/DenseMatrixKernels.h>
#include <TNL/Matrices/DenseMatrix.h>
#include <TNL/Matrices/MatrixOperations.h>
#include <TNL/Matrices/DenseMatrixGenerator.h>
#include "MagmaBenchmark.h"
#include "CublasBenchmark.h"

namespace TNL::Benchmarks::DenseMatrices {

template <typename Real = double, typename Index = int>
struct DenseMatricesBenchmark {
    using RealType = Real;
    using IndexType = Index;
    using DeviceType = TNL::Devices::Cuda::CudaDevice;

    static void configSetup(TNL::Config::ConfigDescription& config) {
        config.addDelimiter("Benchmark settings:");
        config.addEntry<TNL::String>("input-file", "Input file with dense matrices.");
        config.addEntry<TNL::String>("log-file", "Log file name.", "tnl-benchmark-dense-matrices.log");
        config.addEntry<TNL::String>("output-mode", "Mode for opening the log file.", "overwrite");
        config.addEntryEnum("append");
        config.addEntryEnum("overwrite");

        config.addDelimiter("Device settings:");
        config.addEntry<TNL::String>("device", "Device the computation will run on.", "cuda");
        config.addEntryEnum<TNL::String>("cuda");
        TNL::Devices::Cuda::Cublas::configSetup(config);
        TNL::Devices::Cuda::Magma::configSetup(config);

        config.addEntry<int>("loops", "Number of iterations for every computation.", 10);
        config.addEntry<int>("verbose", "Verbose mode.", 1);
    }

    DenseMatricesBenchmark(const TNL::Config::ParameterContainer& parameters_) : parameters(parameters_) {}

    void runBenchmark() {
        auto inputFile = parameters.getParameter<TNL::String>("input-file");
        const TNL::String logFileName = parameters.getParameter<TNL::String>("log-file");
        const TNL::String outputMode = parameters.getParameter<TNL::String>("output-mode");
        const int loops = parameters.getParameter<int>("loops");
        const int verbose = parameters.getParameter<int>("verbose");

        auto mode = std::ios::out;
        if (outputMode == "append")
            mode |= std::ios::app;
        std::ofstream logFile(logFileName.getString(), mode);
        TNL::Benchmarks::Benchmark<> benchmark(logFile, loops, verbose);

        // Write global metadata into a separate file
        std::map<std::string, std::string> metadata = TNL::Benchmarks::getHardwareMetadata();
        TNL::Benchmarks::writeMapAsJson(metadata, logFileName, ".metadata.json");

        this->errors = 0;

        TNL::String device = parameters.getParameter<TNL::String>("device");
        DeviceType::initializeDevice();

        std::cout << "Dense Matrices benchmark with " << TNL::getType<Real>() << " precision and device: " << device
                  << std::endl;

        // Read dense matrices from the input file
        TNL::Matrices::DenseMatrix<RealType, IndexType> denseMatrix;
        TNL::Matrices::DenseMatrixGenerator::readMatrix(inputFile, denseMatrix);

        if (device == "cuda") {
            // Benchmark using CUDA with Magma library
            CublasBenchmark<RealType, IndexType> cublasBenchmark(denseMatrix, benchmark);
            cublasBenchmark.runBenchmark();

            MagmaBenchmark<RealType, IndexType> magmaBenchmark(denseMatrix, benchmark);
            magmaBenchmark.runBenchmark();
        } else if (device == "cublas") {
            // Benchmark using CUDA with Cublas library
            CublasBenchmark<RealType, IndexType> cublasBenchmark(denseMatrix, benchmark);
            cublasBenchmark.runBenchmark();
        } else if (device == "magma") {
            // Benchmark using CUDA with Magma library
            MagmaBenchmark<RealType, IndexType> magmaBenchmark(denseMatrix, benchmark);
            magmaBenchmark.runBenchmark();
        }
    }
};

}  // namespace TNL::Benchmarks::DenseMatrices
