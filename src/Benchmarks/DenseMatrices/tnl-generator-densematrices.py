#!/usr/bin/python3

import numpy as np
from scipy import io as sio

def generate_dense_matrix(size):
    if size <= 0:
        print("Size must be a positive integer.")
        return None
    else:
        print(f"Generating a dense matrix of size {size}")
        matrix = np.random.rand(size, size)  # Create a random dense matrix
        return matrix

def save_matrix_to_file(matrix, size):
    if matrix is not None:
        try:
            print(f"Writing file matrix-{size}.mtx", end="\r")
            sio.mmwrite(f"matrix-{size}.mtx", matrix)
            print("Matrix generation and saving complete.")
        except Exception as e:
            print(f"Error while saving the matrix: {e}")

def main():
    size = int(input("Enter the size of the matrix: "))
    dense_matrix = generate_dense_matrix(size)
    if dense_matrix is not None:
        save_matrix_to_file(dense_matrix, size)

if __name__ == "__main__":
    main()
