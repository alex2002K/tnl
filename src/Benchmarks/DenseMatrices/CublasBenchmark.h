#pragma once

#include <cstddef>

// Mocked cuBLAS functions and data structures
typedef enum {
    CUBLAS_STATUS_SUCCESS,
} cublasStatus_t;

typedef struct {
    int dummy;
} cublasHandle_t;

cublasStatus_t cublasCreate(cublasHandle_t* handle);
cublasStatus_t cublasDestroy(cublasHandle_t handle);

cublasStatus_t cublasSgemm(cublasHandle_t handle, char transa, char transb, int m, int n, int k, const float* alpha, const float* A, int lda, const float* B, int ldb, const float* beta, float* C, int ldc);
